$(document).ready(function(){



//init func
sectionPic();
parallaxSection();

});

//
function sectionPic(){
	if($('.js-section-anch').get(0)){
		$('.js-section-anch').each(function(){
			var targetElem = $(this),
				windowH = $(window).height(),
				targetElemPos = targetElem.offset().top - (windowH / 2),
				targetElemHeight = targetElem.height();
			$(window).resize(function(){
				windowH = $(window).height(),
				targetElemPos = targetElem.offset().top - (windowH / 2),
				targetElemHeight = targetElem.height();
			});
			$(window).scroll(function(){
				var scrolledAtop = $(this).scrollTop();
				if(scrolledAtop >= targetElemPos && scrolledAtop <= targetElemPos + targetElemHeight){
					$('.section-pic--active').removeClass('section-pic--active');
					targetElem.addClass('section-pic--active');
				}
			});
		});
	}
}

//
function parallaxSection(){
	if($('.js-parallax-section').get(0)){
		$('.js-parallax-section').each(function(){
			var section = $(this),
				windowHeight = $(window).height(),
				sectionPos = section.offset().top,
				parallaxItem = section.find('.parallax-item');
			$(window).resize(function(){
				windowHeight = $(window).height(),
				sectionPos = section.offset().top;
			});
			parallaxItem.each(function(){
				var parallaxItemThis = $(this);
				$(window).scroll(function(){
					var scrTop = $(window).scrollTop();
					if(scrTop >= sectionPos - windowHeight){
						var yPos = -((scrTop - sectionPos) / parallaxItemThis.data('speed')),
							coords = 'translate3d(0px, ' + yPos + 'px, 0px)';
						parallaxItemThis.css('transform', coords);
					}
				});
			});
		});
	}
}




